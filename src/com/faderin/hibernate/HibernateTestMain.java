package com.faderin.hibernate;

import java.util.List;

public class HibernateTestMain {
    public static void main(String[] args) {
        ConnectHibernate ch = ConnectHibernate.getInstance();
        List<Membership> members = ch.getMembers();
        for (Membership m : members) {
            System.out.println(m);
        }

        System.out.println("---------------------Get Member with ID-------------------------");

        System.out.println(ch.getMember(1));
    }
}

