package com.faderin.hibernate;
import javax.persistence.*;

@Entity
@Table(name = "membership")
public class Membership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Membership_id")
    private int Membership_id;

    @Column(name = "RecordNumber")
    private int RecordNumber;

    @Column(name = "First_Name")
    private String First_Name;

    @Column(name = "Last_Name")
    private String Last_Name;

    @Column(name = "BirthDate")
    private String BirthDate;

    @Column(name = "Gender")
    private String Gender;

    @Column(name = "BirthPlace")
    private String BirthPlace;

    @Column(name = "BirthCountry")
    private String BirthCountry;

    @Column(name = "HouseHold_id")
    private String HouseHold_id;

    @Column(name = "Ward_id")
    private int Ward_id;

    @Column(name = "MaritalStatus")
    private String MaritalStatus;

    public int getMembership_id() {
        return Membership_id;
    }

    public void setMembership_id(int membership_id) {
        Membership_id = membership_id;
    }

    public int getRecordNumber() {
        return RecordNumber;
    }

    public void setRecordNumber(int recordNumber) {
        RecordNumber = recordNumber;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getBirthPlace() {
        return BirthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        BirthPlace = birthPlace;
    }

    public String getBirthCountry() {
        return BirthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        BirthCountry = birthCountry;
    }

    public String getHouseHold_id() {
        return HouseHold_id;
    }

    public void setHouseHold_id(String houseHold_id) {
        HouseHold_id = houseHold_id;
    }

    public int getWard_id() {
        return Ward_id;
    }

    public void setWard_id(int ward_id) {
        Ward_id = ward_id;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    @Override
    public String toString() {
        return "Membership{" +
                "Membership_id=" + Membership_id +
                ", RecordNumber=" + RecordNumber +
                ", First_Name='" + First_Name + '\'' +
                ", Last_Name='" + Last_Name + '\'' +
                ", BirthDate=" + BirthDate +
                ", Gender='" + Gender + '\'' +
                ", BirthPlace='" + BirthPlace + '\'' +
                ", BirthCountry='" + BirthCountry + '\'' +
                ", HouseHold_id=" + HouseHold_id +
                ", Ward_id=" + Ward_id +
                ", MaritalStatus='" + MaritalStatus + '\'' +
                '}';
    }
}
