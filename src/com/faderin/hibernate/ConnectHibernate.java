package com.faderin.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.*;

public class ConnectHibernate {
    SessionFactory factory = null;
    Session session = null;

    private static ConnectHibernate single_instance = null;

    private ConnectHibernate() {
        factory = HibernateEngine.getSessionFactory();
    }

    public static ConnectHibernate getInstance() {
        if (single_instance == null) {
            single_instance = new ConnectHibernate();
        }

        return single_instance;
    }

    public List<Membership> getMembers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.faderin.hibernate.Membership";
            List<Membership> cs = (List<Membership>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Membership getMember(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.faderin.hibernate.Membership where Membership_id=" + Integer.toString(id);
            Membership c = (Membership)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}


